﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LazyApp
{
    public partial class CreateProfileForm : Form
    {
        //private static CreateProfileForm instance;

        //public static CreateProfileForm GetFrom
        //{
        //    get
        //    {
        //        if (instance == null || instance.IsDisposed)
        //            instance = new CreateProfileForm();
        //        return instance;
        //    }
        //}

        public CreateProfile profileInst;
        public LazyProcess lazyInst;
        public LaunchPrograms launchInst;

        public CreateProfileForm()
        {
            InitializeComponent();
        }

        public void Init(CreateProfile profile, LazyProcess lazy, LaunchPrograms launch)
        {
            profileInst = profile;
            lazyInst = lazy;
            launchInst = launch;
        }

        private void Create_Click(object sender, EventArgs e)
        {
            DelProfile.Enabled = true;
            lazyInst.File_Select.SelectedItem = profileInst.CreateNewProfile(this);
            SaveProfile.Enabled = true;
            FindProgram.Enabled = true;
            Entry_Paste.Enabled = true;
            EditProfile.Enabled = false;
            Create.Enabled = false;         
        }

        private void EditProfile_Click(object sender, EventArgs e)
        {
            DelProfile.Enabled = true;
            SaveProfile.Enabled = true;
            EditProfile.Enabled = false;
            launchInst.UpdateSelectedProfile();


            ProfileName.Text = "stuff" + (string)lazyInst.File_Select.SelectedItem;

            profileInst.EditProfile();
            FindProgram.Enabled = true;
            Entry_Paste.Enabled = true;

            SaveProfile.Enabled = true;
            FindProgram.Enabled = true;
            Entry_Paste.Enabled = true;
            Create.Enabled = false;
            launchInst.UpdateSelectedProfile();
            ProfileName.Text = (string)lazyInst.File_Select.SelectedItem;
            Entry_Paste.Lines = File.ReadAllLines(launchInst.selectedProfilePath);
            profileInst.EditProfile();
            profileInst.isEditing = true;
        }
        
        private void FindProgram_Click(object sender, EventArgs e)
        {
            profileInst.FindProgram(this);
        }

        private void SaveProfile_Click(object sender, EventArgs e)
        {
            SaveProfile.Enabled = false;

            profileInst.SaveProfile(this, Entry_Paste.Lines);
            //if (openFile != null)
            //    openFile.Close();

            //string[] paths = programsToSave.ToArray();

            //foreach (string path in paths)
            //{
            //    if (path != "")
            //        CreateEntry(path, selectedProfilePath, "Paths");

            //}

            //string[] lines = createProfile.SelectNewURL();

            //foreach (string line in lines)
            //    CreateEntry(line, selectedProfilePath, "URLs");

            Entry_Paste.Clear();
            FindProgram.Enabled = false;
            Entry_Paste.Enabled = false;
            EditProfile.Enabled = true;

            //if (!continueEditing)
            //    isEditing = false;
        }

        private void ProfileName_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void DelProfile_Click(object sender, EventArgs e)
        {
            profileInst.DeleteProfile();
            Entry_Paste.Text = "Insert URLs or paths";
            Entry_Paste.Enabled = false;
            ProfileName.Text = "Name";
            Create.Enabled = true;
            SaveProfile.Enabled = false;
            
            FindProgram.Enabled = false;
            DelProfile.Enabled = false;

            profileInst.RefreshProfiles();
            launchInst.UpdateSelectedProfile();
            EditProfile.Enabled = true;
        }

        private void UpdateProfiles_Click(object sender, EventArgs e)
        {
            profileInst.RefreshProfiles();
        }
    }
}
