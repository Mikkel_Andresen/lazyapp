﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LazyApp
{
    public partial class CreateProfile : Form
    {
        public CreateProfile()
        {
            InitializeComponent();
        }

        public string SelectNewPath(LazyProcess lazyProcess)
        {
            string path = "";

            if (lazyProcess.PathBrowser.ShowDialog() == DialogResult.OK)
            {
                lazyProcess.PathBrowser.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
                path = lazyProcess.PathBrowser.FileName;
            }
            return path;
        }

        public string[] SelectNewURL(LazyProcess lazyProcess)
        {
            var URL = lazyProcess.URL_Paste.Lines;
            return URL;
        }

        public string CreateNewProfile(LazyProcess lazyProcess)
        {
            string name = lazyProcess.ProfileName.Text;

            if (!File.Exists(lazyProcess.profileDir + name + ".txt"))
            {
                using (StreamWriter newFile = File.CreateText(lazyProcess.profileDir + name + ".txt"))
                {
                    newFile.WriteLine("URLs" + Environment.NewLine);
                    newFile.WriteLine("Paths");
                    newFile.Close();
                }
                lazyProcess.RefreshProfiles();
            }
            else
                lazyProcess.DisplayLog("Profile already exists");

            return name;
        }
    }
}
