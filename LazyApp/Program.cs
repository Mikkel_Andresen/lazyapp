﻿using System;
using System.Windows.Forms;

namespace LazyApp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            LazyProcess process = new LazyProcess();
            Application.Run(process);
        }
    }
}
