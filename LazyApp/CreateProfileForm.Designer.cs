﻿namespace LazyApp
{
    partial class CreateProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProfileName = new System.Windows.Forms.TextBox();
            this.Create = new System.Windows.Forms.Button();
            this.SaveProfile = new System.Windows.Forms.Button();
            this.EditProfile = new System.Windows.Forms.Button();
            this.Entry_Paste = new System.Windows.Forms.TextBox();
            this.FindProgram = new System.Windows.Forms.Button();
            this.UpdateProfiles = new System.Windows.Forms.Button();
            this.PathBrowser = new System.Windows.Forms.OpenFileDialog();
            this.DelProfile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ProfileName
            // 
            this.ProfileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfileName.Location = new System.Drawing.Point(12, 11);
            this.ProfileName.Name = "ProfileName";
            this.ProfileName.Size = new System.Drawing.Size(98, 27);
            this.ProfileName.TabIndex = 15;
            this.ProfileName.Text = "Name";
            this.ProfileName.WordWrap = false;
            this.ProfileName.TextChanged += new System.EventHandler(this.ProfileName_TextChanged);
            // 
            // Create
            // 
            this.Create.Location = new System.Drawing.Point(12, 83);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(98, 23);
            this.Create.TabIndex = 16;
            this.Create.Text = "Create Profile";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // SaveProfile
            // 
            this.SaveProfile.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SaveProfile.Enabled = false;
            this.SaveProfile.Location = new System.Drawing.Point(0, 238);
            this.SaveProfile.Name = "SaveProfile";
            this.SaveProfile.Size = new System.Drawing.Size(506, 23);
            this.SaveProfile.TabIndex = 21;
            this.SaveProfile.Text = "Save Profile";
            this.SaveProfile.UseVisualStyleBackColor = true;
            this.SaveProfile.Click += new System.EventHandler(this.SaveProfile_Click);
            // 
            // EditProfile
            // 
            this.EditProfile.Enabled = false;
            this.EditProfile.Location = new System.Drawing.Point(12, 138);
            this.EditProfile.Name = "EditProfile";
            this.EditProfile.Size = new System.Drawing.Size(98, 23);
            this.EditProfile.TabIndex = 18;
            this.EditProfile.Text = "Edit Profile";
            this.EditProfile.UseVisualStyleBackColor = true;
            this.EditProfile.Click += new System.EventHandler(this.EditProfile_Click);
            // 
            // Entry_Paste
            // 
            this.Entry_Paste.AcceptsReturn = true;
            this.Entry_Paste.AcceptsTab = true;
            this.Entry_Paste.AllowDrop = true;
            this.Entry_Paste.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Entry_Paste.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Entry_Paste.Enabled = false;
            this.Entry_Paste.Location = new System.Drawing.Point(116, 12);
            this.Entry_Paste.Multiline = true;
            this.Entry_Paste.Name = "Entry_Paste";
            this.Entry_Paste.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Entry_Paste.Size = new System.Drawing.Size(378, 220);
            this.Entry_Paste.TabIndex = 20;
            this.Entry_Paste.Text = "Insert URLs or paths";
            this.Entry_Paste.WordWrap = false;
            // 
            // FindProgram
            // 
            this.FindProgram.Enabled = false;
            this.FindProgram.Location = new System.Drawing.Point(12, 196);
            this.FindProgram.Name = "FindProgram";
            this.FindProgram.Size = new System.Drawing.Size(98, 23);
            this.FindProgram.TabIndex = 19;
            this.FindProgram.Text = "Find a Program";
            this.FindProgram.UseVisualStyleBackColor = true;
            this.FindProgram.Click += new System.EventHandler(this.FindProgram_Click);
            // 
            // UpdateProfiles
            // 
            this.UpdateProfiles.Location = new System.Drawing.Point(12, 167);
            this.UpdateProfiles.Name = "UpdateProfiles";
            this.UpdateProfiles.Size = new System.Drawing.Size(98, 23);
            this.UpdateProfiles.TabIndex = 17;
            this.UpdateProfiles.Text = "Refresh Profiles";
            this.UpdateProfiles.UseVisualStyleBackColor = true;
            this.UpdateProfiles.Click += new System.EventHandler(this.UpdateProfiles_Click);
            // 
            // PathBrowser
            // 
            this.PathBrowser.FileName = "PathBrowser";
            // 
            // DelProfile
            // 
            this.DelProfile.Enabled = false;
            this.DelProfile.Location = new System.Drawing.Point(12, 109);
            this.DelProfile.Name = "DelProfile";
            this.DelProfile.Size = new System.Drawing.Size(98, 23);
            this.DelProfile.TabIndex = 22;
            this.DelProfile.Text = "Delete Profile";
            this.DelProfile.UseVisualStyleBackColor = true;
            this.DelProfile.Click += new System.EventHandler(this.DelProfile_Click);
            // 
            // CreateProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 261);
            this.Controls.Add(this.DelProfile);
            this.Controls.Add(this.ProfileName);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.SaveProfile);
            this.Controls.Add(this.EditProfile);
            this.Controls.Add(this.Entry_Paste);
            this.Controls.Add(this.FindProgram);
            this.Controls.Add(this.UpdateProfiles);
            this.Name = "CreateProfileForm";
            this.Text = "Create Profile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox ProfileName;
        public System.Windows.Forms.TextBox Entry_Paste;
        private System.Windows.Forms.Button UpdateProfiles;
        public System.Windows.Forms.Button SaveProfile;
        public System.Windows.Forms.Button EditProfile;
        public System.Windows.Forms.Button FindProgram;
        public System.Windows.Forms.OpenFileDialog PathBrowser;
        public System.Windows.Forms.Button Create;
        public System.Windows.Forms.Button DelProfile;
    }
}