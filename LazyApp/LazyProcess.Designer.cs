﻿namespace LazyApp
{
    partial class LazyProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Open_Programs = new System.Windows.Forms.Button();
            this.DisplayConsole = new System.Windows.Forms.TextBox();
            this.Browser_Select = new System.Windows.Forms.ComboBox();
            this.File_Select = new System.Windows.Forms.ComboBox();
            this.RestartProgram = new System.Windows.Forms.Button();
            this.PathBrowser = new System.Windows.Forms.OpenFileDialog();
            this.TopMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.editProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instructionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TopMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // Open_Programs
            // 
            this.Open_Programs.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Open_Programs.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Open_Programs.Location = new System.Drawing.Point(143, 81);
            this.Open_Programs.Name = "Open_Programs";
            this.Open_Programs.Size = new System.Drawing.Size(129, 61);
            this.Open_Programs.TabIndex = 2;
            this.Open_Programs.Text = "Open Programs";
            this.Open_Programs.UseVisualStyleBackColor = true;
            this.Open_Programs.Click += new System.EventHandler(this.Open_Programs_Click);
            // 
            // DisplayConsole
            // 
            this.DisplayConsole.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.DisplayConsole.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.DisplayConsole.Location = new System.Drawing.Point(12, 176);
            this.DisplayConsole.Multiline = true;
            this.DisplayConsole.Name = "DisplayConsole";
            this.DisplayConsole.ReadOnly = true;
            this.DisplayConsole.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DisplayConsole.Size = new System.Drawing.Size(260, 71);
            this.DisplayConsole.TabIndex = 1;
            this.DisplayConsole.TabStop = false;
            // 
            // Browser_Select
            // 
            this.Browser_Select.FormattingEnabled = true;
            this.Browser_Select.Location = new System.Drawing.Point(12, 54);
            this.Browser_Select.Name = "Browser_Select";
            this.Browser_Select.Size = new System.Drawing.Size(260, 21);
            this.Browser_Select.TabIndex = 1;
            // 
            // File_Select
            // 
            this.File_Select.FormattingEnabled = true;
            this.File_Select.Location = new System.Drawing.Point(12, 27);
            this.File_Select.Name = "File_Select";
            this.File_Select.Size = new System.Drawing.Size(260, 21);
            this.File_Select.TabIndex = 0;
            this.File_Select.SelectedIndexChanged += new System.EventHandler(this.File_Select_SelectedIndexChanged);
            // 
            // RestartProgram
            // 
            this.RestartProgram.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.RestartProgram.Enabled = false;
            this.RestartProgram.Location = new System.Drawing.Point(12, 81);
            this.RestartProgram.Name = "RestartProgram";
            this.RestartProgram.Size = new System.Drawing.Size(132, 61);
            this.RestartProgram.TabIndex = 14;
            this.RestartProgram.TabStop = false;
            this.RestartProgram.Text = "Restart Unity";
            this.RestartProgram.UseVisualStyleBackColor = true;
            this.RestartProgram.Visible = false;
            this.RestartProgram.Click += new System.EventHandler(this.RestartProgram_Click);
            // 
            // PathBrowser
            // 
            this.PathBrowser.FileName = "PathBrowser";
            // 
            // TopMenuStrip
            // 
            this.TopMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.TopMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.TopMenuStrip.Name = "TopMenuStrip";
            this.TopMenuStrip.Size = new System.Drawing.Size(286, 24);
            this.TopMenuStrip.TabIndex = 15;
            this.TopMenuStrip.Text = "TopMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profileNew,
            this.editProfileToolStripMenuItem,
            this.quitToolStripMenuItem,
            this.quitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // profileNew
            // 
            this.profileNew.Name = "profileNew";
            this.profileNew.Size = new System.Drawing.Size(172, 22);
            this.profileNew.Text = "Create New Profile";
            this.profileNew.Click += new System.EventHandler(this.profileNew_Click);
            // 
            // editProfileToolStripMenuItem
            // 
            this.editProfileToolStripMenuItem.Name = "editProfileToolStripMenuItem";
            this.editProfileToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.editProfileToolStripMenuItem.Text = "Edit Profile";
            this.editProfileToolStripMenuItem.Click += new System.EventHandler(this.editProfileToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(169, 6);
            // 
            // quitToolStripMenuItem1
            // 
            this.quitToolStripMenuItem1.Name = "quitToolStripMenuItem1";
            this.quitToolStripMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.quitToolStripMenuItem1.Text = "Quit";
            this.quitToolStripMenuItem1.Click += new System.EventHandler(this.quitToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instructionsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // instructionsToolStripMenuItem
            // 
            this.instructionsToolStripMenuItem.Name = "instructionsToolStripMenuItem";
            this.instructionsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.instructionsToolStripMenuItem.Text = "Instructions";
            this.instructionsToolStripMenuItem.Click += new System.EventHandler(this.instructionsToolStripMenuItem_Click);
            // 
            // LazyProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(286, 259);
            this.Controls.Add(this.RestartProgram);
            this.Controls.Add(this.File_Select);
            this.Controls.Add(this.Browser_Select);
            this.Controls.Add(this.DisplayConsole);
            this.Controls.Add(this.Open_Programs);
            this.Controls.Add(this.TopMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.TopMenuStrip;
            this.Name = "LazyProcess";
            this.Text = "LazyApp";
            this.TopMenuStrip.ResumeLayout(false);
            this.TopMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Open_Programs;
        private System.Windows.Forms.TextBox DisplayConsole;
        private System.Windows.Forms.Button RestartProgram;
        public System.Windows.Forms.OpenFileDialog PathBrowser;
        private System.Windows.Forms.MenuStrip TopMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profileNew;
        private System.Windows.Forms.ToolStripSeparator quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem1;
        public System.Windows.Forms.ComboBox Browser_Select;
        public System.Windows.Forms.ComboBox File_Select;
        private System.Windows.Forms.ToolStripMenuItem editProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instructionsToolStripMenuItem;
    }
}

