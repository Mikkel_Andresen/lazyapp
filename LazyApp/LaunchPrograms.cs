﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

namespace LazyApp
{
    public class LaunchPrograms
    {
        // Browser selection drop down list
        public enum browsers { Chrome, Edge, Opera, FireFox, IExplorer };
        public browsers curBrowser = browsers.Chrome;
        public string SelectedBrowser = "";

        // Selecting profile with links and programs to run
        public string selectedProfile = "";
        public string selectedProfilePath = "";
        private List<string> programsToRun = new List<string>();

        // LazyApp
        private bool bDebug = false;
        private bool bRunOnce = false;
        private bool bOpenBrowser = true;
        private string[] settings;
        private string settingsPath;

        private LazyProcess lazyInst;
        private CreateProfile profileInst;
        public void Init(LazyProcess lazy, CreateProfile profile)
        {
            lazyInst = lazy;
            profileInst = profile;
        }

        public void Setup()
        {
            // Set Browser_Select (combobox) values to browsers
            lazyInst.Browser_Select.DataSource = Enum.GetValues(typeof(browsers));
            lazyInst.Browser_Select.DropDownStyle = ComboBoxStyle.DropDownList;

            profileInst.RefreshProfiles();

            try
            {
                UpdateSelectedProfile();
            }
            catch (DirectoryNotFoundException dir)
            {
                Debug.Print(dir.Message);
            }

            // Find and store settings file lines
            try
            {
                //settingsPath = Directory.GetCurrentDirectory() + @"\LazyApp\settings.txt";
                settingsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp", "settings.txt");
                settings = File.ReadAllLines(settingsPath);
            }
            catch
            {
                settingsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LazyApp");

                if (!Directory.Exists(settingsPath))
                {
                    Directory.CreateDirectory(settingsPath);
                }

                string filePath = Path.Combine(settingsPath, "settings.txt");

                using (StreamWriter newFile = File.CreateText(filePath))
                {
                    string lines = "bOpenBrowser=1" + Environment.NewLine +
                        "bDebug=1" + Environment.NewLine +
                        "runOnce=0" + Environment.NewLine +
                        "pingURL=0" + Environment.NewLine +
                        "Set to either \"chrome.exe\", \"microsoft - edge:\", \"Opera.exe\", \"firefox.exe\", \"iexplore.exe\"" + Environment.NewLine +
                        "browser = chrome.exe";

                    newFile.Write(lines);
                    newFile.Close();
                }

                Debug.Print("No settings file found, creating new settings file");
            }

            // Run once ?
            bRunOnce = CheckSettings("runOnce", settings) == "1";
            bDebug = CheckSettings("bDebug", settings) == "1";
            bOpenBrowser = CheckSettings("bOpenBrowser", settings) == "1";

            // Set browser to what the settings file specifies
            switch (CheckSettings("browser", settings))
            {
                case "chrome.exe":
                    curBrowser = browsers.Chrome;
                    break;
                case "microsoft - edge:":
                    curBrowser = browsers.Edge;
                    break;
                case "Opera.exe":
                    curBrowser = browsers.Opera;
                    break;
                case "firefox.exe":
                    curBrowser = browsers.FireFox;
                    break;
                case "iexplore.exe":
                    curBrowser = browsers.IExplorer;
                    break;
                default:
                    break;
            }

            lazyInst.Browser_Select.SelectedItem = curBrowser;

            if (bRunOnce)
                Launch();
        }

        public void Launch()
        {
            settings = File.ReadAllLines(settingsPath);

            // Run once ?
            bRunOnce = CheckSettings("runOnce", settings) == "1";
            bDebug = CheckSettings("bDebug", settings) == "1";
            bOpenBrowser = CheckSettings("bOpenBrowser", settings) == "1";
            
            // Sets browser to use
            curBrowser = (browsers)lazyInst.Browser_Select.SelectedItem;

            switch (curBrowser)
            {
                case browsers.Edge:
                    SelectedBrowser = "microsoft - edge:";
                    break;
                case browsers.Chrome:
                    SelectedBrowser = "chrome.exe";
                    break;
                case browsers.Opera:
                    SelectedBrowser = "Opera.exe";
                    break;
                case browsers.FireFox:
                    SelectedBrowser = "firefox.exe";
                    break;
                case browsers.IExplorer:
                    SelectedBrowser = "iexplore.exe";
                    break;
                default:
                    break;
            }

            UpdateSelectedProfile();

            if (CheckDirectoryFile(selectedProfilePath))
            {
                try
                {
                    string[] lines = File.ReadAllLines(selectedProfilePath);

                    foreach (string line in lines)
                    {
                        if (CheckDirectoryFile(line))
                            programsToRun.Add(@line);

                    }

                }
                catch (FileNotFoundException e)
                {
                    lazyInst.DisplayLog(e.Message);
                    profileInst.RefreshProfiles();
                }
            }
            // Sets info for which browser and what URLs to open
            Process browserToOpen = new Process();
            ProcessStartInfo browserInfo = new ProcessStartInfo(SelectedBrowser);

            // Read what web pages to run from the selected profile .txt file
            List<string> args = new List<string>();

            // Add all lines that has a URL which responds
            if (bOpenBrowser)
            {
                try
                {
                    string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp\Profiles", selectedProfilePath);

                    //string[] lines = File.ReadAllLines(Directory.GetCurrentDirectory() + @"\LazyApp\Profiles\" + selectedProfile);
                    string[] lines = File.ReadAllLines(path);

                    foreach (string line in lines)
                    {
                        if (CheckURL(line))
                            args.Add(line);
                    }
                }
                catch
                {
                    //lazyInst.DisplayLog(e.Message);
                }

                browserInfo.Arguments = string.Join(" ", args);
                browserToOpen.StartInfo = browserInfo;
            }

            // Checks for processes with same name already running
            string fileName = Path.GetFileNameWithoutExtension(SelectedBrowser);
            bool canRun = true;
            Process[] allProcesses = Process.GetProcessesByName(fileName);

            if (bOpenBrowser)
            {
                if (allProcesses == null || allProcesses.Length > 0)
                    canRun = false;
                else
                    canRun = true;

                try
                {
                    if (canRun)
                        browserToOpen.Start();
                    else
                        lazyInst.DisplayLog("Browser already running");
                }
                catch
                {
                    lazyInst.DisplayLog("Couldn't find selected browser, select a different one");
                    bRunOnce = false;
                }
            }

            // Run all programs in the list of programs specified through the settings file
            foreach (string program in programsToRun)
            {
                fileName = Path.GetFileNameWithoutExtension(program);
                canRun = true;
                allProcesses = Process.GetProcessesByName(fileName);

                // Don't run the program if it is already running
                if (allProcesses == null || allProcesses.Length > 0)
                    canRun = false;
                else
                    canRun = true;

                try
                {
                    if (canRun)
                    {
                        Process.Start(program);
                        lazyInst.DisplayLog(program);
                    }
                }
                catch
                {
                    lazyInst.DisplayLog("Something went wrong");
                }

            }

            programsToRun.Clear();

            // Stop when program is finished unless debugging
            if (bRunOnce)
                Process.GetCurrentProcess().Kill();
        }

        public void RestartProgram(string name, string path)
        {
            Process[] processes = Process.GetProcessesByName(name);

            foreach (Process p in processes)
            {
                p.Kill();
            }

            if (CheckDirectoryFile(path))
                Process.Start(path);
        }

        // Checks if a file exists at given path
        private bool CheckDirectoryFile(string path)
        {
            try
            {
                return File.Exists(path);
            }
            catch
            {
                return false;
            }
        }

        // Checks URL format or pings the URL for a response
        private bool CheckURL(string url)
        {
            try
            {
                if (CheckSettings("pingURL", settings) == "1")
                {
                    // Test URL properly by making requests (slower, but more thorough)
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = WebRequestMethods.Http.Head;
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    return response.StatusCode == HttpStatusCode.OK;
                }
                else
                {
                    // Test URL by checking its formatting (quicker)
                    return Uri.IsWellFormedUriString(url, UriKind.Absolute);
                }
            }
            catch
            {
                return false;
            }
        }

        // Update the selectedProfile string to the drop down list
        public void UpdateSelectedProfile()
        {
            // Sets which profile (.txt) to use
            //selectedProfile = @"\" + (string)lazyInst.File_Select.SelectedItem + ".txt";
            string path = (string)lazyInst.File_Select.SelectedItem + ".txt";

            //selectedProfile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp\Profiles", path);
            //selectedProfilePath = Directory.GetCurrentDirectory() + @"\LazyApp\Profiles" + selectedProfile;
            selectedProfilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp\Profiles", path);
            Debug.Print(selectedProfilePath);
        }

        // Read settings file and find result to return
        public string CheckSettings(string setting, string[] settingsToRead)
        {
            string condition = "";

            try
            {
                foreach (string line in settingsToRead)
                {
                    if (line.Contains(setting))
                    {
                        int index = line.IndexOf("=") + 1;
                        condition = line.Substring(index);
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Debug.Print(e.Message);
            }
            return condition;
        }
    }
}
