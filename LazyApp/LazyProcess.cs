﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net;

namespace LazyApp
{
    public partial class LazyProcess : Form
    {
        // Classes
        public CreateProfileForm createProfileWindow;
        public LaunchPrograms launchInst = new LaunchPrograms();
        public CreateProfile profileInst = new CreateProfile();

        public LazyProcess()
        {
            InitializeComponent();
            profileInst.Init(this, launchInst);
            launchInst.Init(this, profileInst);

            launchInst.Setup();
        }

        private void File_Select_SelectedIndexChanged(object sender, EventArgs e)
        {
            launchInst.UpdateSelectedProfile();

            if (createProfileWindow != null)
            {
                if ((string)File_Select.SelectedItem != createProfileWindow.ProfileName.Text)
                    createProfileWindow.EditProfile.Enabled = true;
                else
                    createProfileWindow.EditProfile.Enabled = false;

                if (profileInst.isEditing)
                {
                    profileInst.EditProfile();
                }
            }
        }

        private void Open_Programs_Click(object sender, EventArgs e)
        {
            launchInst.Launch();
        }

        public void DisplayLog(string whatToWrite)
        {
            DisplayConsole.AppendText(whatToWrite + Environment.NewLine);
        }

        public void ClearLog()
        {
            DisplayConsole.Clear();
        }

        private void RestartProgram_Click(object sender, EventArgs e)
        {
            launchInst.RestartProgram("Unity", @"C:\Program Files\Unity\Editor\Unity.exe");
        }

        private void quitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void profileNew_Click(object sender, EventArgs e)
        {
            if (createProfileWindow != null)
            {
                createProfileWindow.Close();
                createProfileWindow = null;
            }

            createProfileWindow = new CreateProfileForm();
            createProfileWindow.Init(profileInst, this, launchInst);
            createProfileWindow.Create.Enabled = true;
            createProfileWindow.Show();
        }

        private void editProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (File_Select.Items.Count <= 0)
                return;

            if (createProfileWindow != null)
            {
                createProfileWindow.Close();
                createProfileWindow = null;
            }

            if (createProfileWindow == null)
            {
                createProfileWindow = new CreateProfileForm();
                createProfileWindow.Show();
                createProfileWindow.Init(profileInst, this, launchInst);
            }

            createProfileWindow.SaveProfile.Enabled = true;
            createProfileWindow.FindProgram.Enabled = true;
            createProfileWindow.Entry_Paste.Enabled = true;
            createProfileWindow.Create.Enabled = false;
            createProfileWindow.DelProfile.Enabled = true;
            launchInst.UpdateSelectedProfile();
            createProfileWindow.ProfileName.Text = (string)File_Select.SelectedItem;
            createProfileWindow.Entry_Paste.Lines = File.ReadAllLines(launchInst.selectedProfilePath);
            profileInst.EditProfile();
            profileInst.isEditing = true;
        }

        // Open readme file
        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // open readme file
            //string readmePath = Directory.GetCurrentDirectory() + "\\ReadMe.txt";
            string readmePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp", "ReadMe.txt");
            try
            {
                Process.Start(readmePath);

            }
            catch (Win32Exception m)
            {
                Debug.Print("No readme file: " + m.Message);
            }
        }
    }
}