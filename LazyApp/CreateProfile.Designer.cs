﻿namespace LazyApp
{
    partial class CreateProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProfileCreator = new System.Windows.Forms.Label();
            this.ProfileName = new System.Windows.Forms.TextBox();
            this.Create = new System.Windows.Forms.Button();
            this.SaveProfile = new System.Windows.Forms.Button();
            this.EditProfile = new System.Windows.Forms.Button();
            this.URL_Paste = new System.Windows.Forms.TextBox();
            this.FindProgram = new System.Windows.Forms.Button();
            this.UpdateProfiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ProfileCreator
            // 
            this.ProfileCreator.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfileCreator.Location = new System.Drawing.Point(44, 28);
            this.ProfileCreator.Name = "ProfileCreator";
            this.ProfileCreator.Size = new System.Drawing.Size(197, 28);
            this.ProfileCreator.TabIndex = 22;
            this.ProfileCreator.Text = "Create new Profile";
            this.ProfileCreator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ProfileName
            // 
            this.ProfileName.Location = new System.Drawing.Point(44, 63);
            this.ProfileName.Name = "ProfileName";
            this.ProfileName.Size = new System.Drawing.Size(98, 20);
            this.ProfileName.TabIndex = 15;
            this.ProfileName.Text = "Name";
            this.ProfileName.WordWrap = false;
            // 
            // Create
            // 
            this.Create.Location = new System.Drawing.Point(143, 60);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(98, 23);
            this.Create.TabIndex = 16;
            this.Create.Text = "Create";
            this.Create.UseVisualStyleBackColor = true;
            // 
            // SaveProfile
            // 
            this.SaveProfile.Enabled = false;
            this.SaveProfile.Location = new System.Drawing.Point(44, 210);
            this.SaveProfile.Name = "SaveProfile";
            this.SaveProfile.Size = new System.Drawing.Size(197, 23);
            this.SaveProfile.TabIndex = 21;
            this.SaveProfile.Text = "Save Profile";
            this.SaveProfile.UseVisualStyleBackColor = true;
            // 
            // EditProfile
            // 
            this.EditProfile.Location = new System.Drawing.Point(143, 89);
            this.EditProfile.Name = "EditProfile";
            this.EditProfile.Size = new System.Drawing.Size(98, 23);
            this.EditProfile.TabIndex = 18;
            this.EditProfile.Text = "Edit Profile";
            this.EditProfile.UseVisualStyleBackColor = true;
            // 
            // URL_Paste
            // 
            this.URL_Paste.AcceptsReturn = true;
            this.URL_Paste.AcceptsTab = true;
            this.URL_Paste.AllowDrop = true;
            this.URL_Paste.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.URL_Paste.Enabled = false;
            this.URL_Paste.Location = new System.Drawing.Point(44, 143);
            this.URL_Paste.Multiline = true;
            this.URL_Paste.Name = "URL_Paste";
            this.URL_Paste.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.URL_Paste.Size = new System.Drawing.Size(197, 61);
            this.URL_Paste.TabIndex = 20;
            this.URL_Paste.Text = "Insert URLs here";
            this.URL_Paste.WordWrap = false;
            // 
            // FindProgram
            // 
            this.FindProgram.Enabled = false;
            this.FindProgram.Location = new System.Drawing.Point(44, 116);
            this.FindProgram.Name = "FindProgram";
            this.FindProgram.Size = new System.Drawing.Size(197, 23);
            this.FindProgram.TabIndex = 19;
            this.FindProgram.Text = "Find a Program";
            this.FindProgram.UseVisualStyleBackColor = true;
            // 
            // UpdateProfiles
            // 
            this.UpdateProfiles.Location = new System.Drawing.Point(44, 89);
            this.UpdateProfiles.Name = "UpdateProfiles";
            this.UpdateProfiles.Size = new System.Drawing.Size(98, 23);
            this.UpdateProfiles.TabIndex = 17;
            this.UpdateProfiles.Text = "Refresh Profiles";
            this.UpdateProfiles.UseVisualStyleBackColor = true;
            // 
            // CreateProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ProfileCreator);
            this.Controls.Add(this.ProfileName);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.SaveProfile);
            this.Controls.Add(this.EditProfile);
            this.Controls.Add(this.URL_Paste);
            this.Controls.Add(this.FindProgram);
            this.Controls.Add(this.UpdateProfiles);
            this.Name = "CreateProfileForm";
            this.Text = "Create Profile";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ProfileCreator;
        public System.Windows.Forms.TextBox ProfileName;
        private System.Windows.Forms.Button Create;
        private System.Windows.Forms.Button SaveProfile;
        private System.Windows.Forms.Button EditProfile;
        public System.Windows.Forms.TextBox URL_Paste;
        private System.Windows.Forms.Button FindProgram;
        private System.Windows.Forms.Button UpdateProfiles;
    }
}