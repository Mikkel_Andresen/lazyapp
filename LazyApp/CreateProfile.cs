﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace LazyApp
{
    public partial class CreateProfile
    {
        // Profiles
        private string[] profilePaths;
        public static string profileDir = "";
        string[] profileNames;

        // Create new profile
        private List<string> programsToSave = new List<string>();
        FileStream openFile;
        public bool isEditing = false;
        private bool continueEditing = false;

        private LazyProcess lazyInst;
        private LaunchPrograms launchInst;

        public void Init(LazyProcess lazy, LaunchPrograms launch)
        {
            lazyInst = lazy;
            launchInst = launch;
            profileDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp\Profiles\");
            //profileDir = Directory.GetCurrentDirectory() + @"\LazyApp\Profiles\";
        }

        public string SelectNewPath(CreateProfileForm profileWindow)
        {
            string path = "";

            if (profileWindow.PathBrowser.ShowDialog() == DialogResult.OK)
            {
                profileWindow.PathBrowser.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
                path = profileWindow.PathBrowser.FileName;
                lazyInst.DisplayLog(path);
            }
            return path;
        }

        //public string[] SelectNewURL()
        //{
        //    var URL = lazyInst.URL_Paste.Lines;
        //    return URL;
        //}

        public string CreateNewProfile(CreateProfileForm profileWindow)
        {
            if (openFile != null)
                openFile.Close();

            isEditing = true;
            launchInst.UpdateSelectedProfile();

            try
            {
                openFile = File.Open(lazyInst.launchInst.selectedProfilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            }
            catch
            {
                Debug.Print("Could not find selected profile");
            }

            string name = profileWindow.ProfileName.Text;

            lazyInst.DisplayLog(profileDir);

            string filePath = profileDir + name + ".txt";

            if (!File.Exists(filePath))
            {
                using (StreamWriter newFile = File.CreateText(filePath))
                {
                    newFile.WriteLine("URLs" + Environment.NewLine);
                    newFile.WriteLine("Paths");
                    newFile.Close();
                }
                RefreshProfiles();
            }
            else
                lazyInst.DisplayLog("Profile already exists");

            try
            {
                profileWindow.Entry_Paste.Lines = File.ReadAllLines(filePath);
            }
            catch (IOException e)
            {
                lazyInst.DisplayLog("This went wrong: " + e.Message);
            }

            return name;
        }

        public void EditProfile()
        {
            isEditing = true;
            launchInst.UpdateSelectedProfile();

            try
            {
                openFile = File.Open(launchInst.selectedProfilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            catch (FileNotFoundException m)
            {
                Debug.Print("Could not find selected profile" + m.Message);
            }
        }

        public void SaveProfile(CreateProfileForm profileWindow, string[] Lines)
        {
            if (openFile != null)
                openFile.Close();

            ReWriteTxtFile(Lines, launchInst.selectedProfilePath);

            if (!continueEditing)
                isEditing = false;

            lazyInst.createProfileWindow = null;
            profileWindow.Close();
        }

        // Delete profile selected in the drop down list
        public void DeleteProfile()
        {
            try
            {
                string filename = launchInst.selectedProfilePath;
                if (File.Exists(filename))
                {
                    openFile.Close();
                    File.Delete(filename);
                }
                else
                {
                    Debug.WriteLine("File does not exist.");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void FindProgram(CreateProfileForm profileWindow)
        {
            string entry = SelectNewPath(profileWindow);
            programsToSave.Add(entry);
            CreateTextBoxEntry(entry, profileWindow.Entry_Paste, "Paths");
        }

        public void ReWriteTxtFile(string[] newLines, string filePath)
        {
            try
            {
                File.WriteAllLines(filePath, newLines);   //Fill a list with the lines from the txt file
                Debug.Print("Save path == " + filePath);
            }
            catch (IOException e)
            {
                lazyInst.DisplayLog(e.Message);
                return;
            }
        }

        public void RefreshProfiles()
        {
            StreamWriter tmpFile = null;
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\LazyApp\Profiles\");

            if (Directory.Exists(path))
                profileDir = path;
            else
            {
                Directory.CreateDirectory(path);
                profileDir = path;
            }

            if (Directory.GetFiles(profileDir, "*.txt").Length != 0)
                profilePaths = Directory.GetFiles(profileDir, "*.txt");
            else
                tmpFile = File.CreateText(profileDir + "Empty Profile.txt");

            if (tmpFile != null)
                tmpFile.Close();

            profilePaths = Directory.GetFiles(profileDir, "*.txt");

            profileNames = new string[profilePaths.Length];
            for (int i = 0; i < profilePaths.Length; i++)
            {
                profileNames[i] = Path.GetFileNameWithoutExtension(profilePaths[i]);
            }

            string tmpPath = "";
            string tmpName = "";
            int tmpIndex = 0;


            if (isEditing)
            {
                tmpPath = launchInst.selectedProfilePath;
                tmpName = launchInst.selectedProfile = (string)lazyInst.File_Select.SelectedItem;
                tmpIndex = lazyInst.File_Select.SelectedIndex;
            }

            lazyInst.File_Select.DataSource = profileNames;
            lazyInst.File_Select.DropDownStyle = ComboBoxStyle.DropDownList;
            launchInst.UpdateSelectedProfile();

            if (isEditing)
            {
                launchInst.selectedProfilePath = tmpPath;
                launchInst.selectedProfile = tmpName;

                if(tmpIndex < lazyInst.File_Select.Items.Count)
                    lazyInst.File_Select.SelectedIndex = tmpIndex;
            }
        }

        public void CreateTextBoxEntry(string line, TextBox box, string whereToInsert)
        {
            List<string> txtLines = new List<string>();

            txtLines = box.Lines.ToList();

            if (txtLines.Contains(whereToInsert))
                txtLines.Insert(txtLines.IndexOf(whereToInsert) + 1, line);  //Insert the line you want to add last under the tag
            else
                txtLines.Insert(0, line);

            box.Lines = txtLines.ToArray();
        }

        public void CreateEntry(string line, string filePath, string whereToInsert)
        {
            if (line == whereToInsert || line == "Paths" || line == "URLs")
                return;

            // Add all lines of a txt file to a list, insert new line after the tag and write the whole txt file over again
            List<string> txtLines = new List<string>();

            try
            {
                txtLines = File.ReadAllLines(filePath).ToList();   //Fill a list with the lines from the txt file
            }
            catch (IOException e)
            {
                lazyInst.DisplayLog(e.Message);
                return;
            }

            // Check if tag exists, if it doesn't, put the line at the top of the file
            if (txtLines.Contains(whereToInsert))
                txtLines.Insert(txtLines.IndexOf(whereToInsert) + 1, line);  //Insert the line you want to add last under the tag
            else
                txtLines.Insert(0, line);

            File.WriteAllLines(filePath, txtLines);                //Add the lines including the new one
        }
    }
}
