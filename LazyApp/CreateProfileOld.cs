﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net;

namespace LazyApp
{
    public partial class CreateProfile
    {
        private readonly LazyProcess lazyProcess;

        public CreateProfile(LazyProcess lazyProcess)
        {
            this.lazyProcess = lazyProcess;
        }

        public string SelectNewPath()
        {
            string path = "";

            if (lazyProcess.PathBrowser.ShowDialog() == DialogResult.OK)
            {
                lazyProcess.PathBrowser.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
                path = lazyProcess.PathBrowser.FileName;
            }
            return path;
        }

        public string[] SelectNewURL()
        {
            var URL = lazyProcess.URL_Paste.Lines;
            return URL;
        }

        public string CreateNewProfile()
        {
            string name = lazyProcess.ProfileName.Text;

            if (!File.Exists(lazyProcess.profileDir + name + ".txt"))
            {
                using (StreamWriter newFile = File.CreateText(lazyProcess.profileDir + name + ".txt"))
                {
                    newFile.WriteLine("URLs" + Environment.NewLine);
                    newFile.WriteLine("Paths");
                    newFile.Close();
                }
                lazyProcess.RefreshProfiles();
            }
            else
                lazyProcess.DisplayLog("Profile already exists");

            return name;
        }
    }
}
